package gui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Controller {

    @FXML private HBox homeButton;
    @FXML private HBox droneButton;
    @FXML private HBox farmButton;
    @FXML private HBox cameraButton;

    @FXML
    public void handleScene(MouseEvent event) {
        try {
            Parent root;
            HBox src = (HBox) event.getSource();
            Stage s = (Stage) src.getScene().getWindow();

            if (src == homeButton) {
                root = FXMLLoader.load(getClass().getResource("xml/house.fxml"));
                s.setScene(new Scene(root, Size.GLOBAL_WIDTH, Size.GLOBAL_HEIGHT));
                s.setTitle("House Controller");
            } else if (src == droneButton) {
                root = FXMLLoader.load(getClass().getResource("xml/drone.fxml"));
                s.setScene(new Scene(root, Size.GLOBAL_WIDTH, Size.GLOBAL_HEIGHT));
                s.setTitle("Drone Controller");
            } else if (src == farmButton) {
                root = FXMLLoader.load(getClass().getResource("xml/farm.fxml"));
                s.setScene(new Scene(root, Size.GLOBAL_WIDTH, Size.GLOBAL_HEIGHT));
                s.setTitle("Farm Controller");
            } else if (src == cameraButton) {
                root = FXMLLoader.load(getClass().getResource("xml/camera.fxml"));
                s.setScene(new Scene(root, Size.GLOBAL_WIDTH, Size.GLOBAL_HEIGHT));
                s.setTitle("Camera Controller");
            }
            s.centerOnScreen();
        } catch (Exception unused) {
            System.exit(39);
        }
    }

    @FXML private AnchorPane room1;
    @FXML private AnchorPane room2;
    @FXML private AnchorPane room3;
    @FXML private AnchorPane room4;
    @FXML private AnchorPane room5;
    @FXML private AnchorPane room6;
    @FXML private Label roomNum;

    @FXML
    public void handleHouseRoom(MouseEvent event) {
        try {
            AnchorPane src = (AnchorPane) event.getSource();
            if (src == room1) {
                roomNum.setText("1 ->");
            } else if (src == room2) {
                roomNum.setText("2 ->");
            } else if (src == room3) {
                roomNum.setText("3 ->");
            } else if (src == room4) {
                roomNum.setText("4 ->");
            } else if (src == room5) {
                roomNum.setText("5 ->");
            } else if (src == room6) {
                roomNum.setText("6 ->");
            }
        } catch (Exception unused) {
            System.exit(234);
        }
    }

    @FXML
    public void handleBackButton(MouseEvent event) {
        try {
            Parent main = FXMLLoader.load(getClass().getResource("xml/main_menu.fxml"));
            Stage s = (Stage) ((ImageView) event.getSource()).getScene().getWindow();
            s.setScene(new Scene(main, Size.MENU_WIDTH, Size.MENU_HEIGHT));
            s.centerOnScreen();
        } catch (Exception unused) {
            System.exit(231);
        }
    }
}
