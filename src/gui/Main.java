package gui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setTitle("Farm controller 2.0");
        primaryStage.centerOnScreen();
        primaryStage.setResizable(false);
        primaryStage.setFullScreen(false);
        primaryStage.setOnCloseRequest(e -> {
            System.out.print("bye!");
            Platform.exit();
        });

        Parent menu = FXMLLoader.load(getClass().getResource("xml/main_menu.fxml"));

        primaryStage.setScene(new Scene(menu, Size.MENU_WIDTH, Size.MENU_HEIGHT));
        primaryStage.show();
    }
}
