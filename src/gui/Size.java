package gui;

public final class Size {

    private Size() {
    }

    public static final int MENU_WIDTH = 600;
    public static final int MENU_HEIGHT = 350;

    public static final int GLOBAL_WIDTH = 1080;
    public static final int GLOBAL_HEIGHT = 720;
}
